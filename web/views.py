# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import JsonResponse
from django.template.loader import render_to_string
from web.models import *

# Create your views here.


def index(request):

    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'index.html', {'context': context})
    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('index.html', {'context': context})
        return JsonResponse({'response': html})


def ourstory(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'ourstory.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('ourstory.html', {'context': context})
        return JsonResponse({'response': html})


def ourgenes(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'ourgenes.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('ourgenes.html', {'context': context})
        return JsonResponse({'response': html})

def values(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'values.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('values.html', {'context': context})
        return JsonResponse({'response': html})

def history(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'history.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('history.html', {'context': context})
        return JsonResponse({'response': html})

def misionvision(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'misionvision.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('misionvision.html', {'context': context})
        return JsonResponse({'response': html})

#pacomarca
def pacomarca(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'pacomarca.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('pacomarca.html', {'context': context})
        return JsonResponse({'response': html})

def herdsmanhut(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'herdsmanhut.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('herdsmanhut.html', {'context': context})
        return JsonResponse({'response': html})

def incashearing(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'incashearing.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('incashearing.html', {'context': context})
        return JsonResponse({'response': html})

def blackalpaca(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'blackalpaca.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('blackalpaca.html', {'context': context})
        return JsonResponse({'response': html})

def geneticprogram(request):
    if request.method == 'GET':
        context = {
            'Events' : 'event',
        }
        return render(request, 'geneticprogram.html', {'context': context})

    if request.method == 'POST':
        context = {
            'Events' : 'event',
        }
        html = render_to_string('geneticprogram.html', {'context': context})
        return JsonResponse({'response': html})







def mainpage(request, page):
    if request.method == 'GET':
        page = Page.objects.get(menuname=page)
        subpages = Subpage.objects.filter(subpage=page)

        context = {
            'subpages' : subpages,
            'page' : page
        }
        return render(request, 'mainpage.html', {'context': context})

    if request.method == 'POST':
        page = Page.objects.get(menuname=page)
        subpages = Subpage.objects.filter(subpage=page)

        context = {
            'subpages': subpages,
            'page': page
        }
        html = render_to_string('mainpage.html', {'context': context})
        return JsonResponse({'response': html})

def subpage(request, page, subpage):
    if request.method == 'GET':
        page = Subpage.objects.get(menuname=subpage)
        subpages = PageContent.objects.filter(subpage=page)

        context = {
            'subpages': subpages,
            'page': page
        }
        return render(request, 'subpage.html', {'context': context})

    if request.method == 'POST':
        page = Subpage.objects.get(menuname=subpage)
        subpages = PageContent.objects.filter(subpage=page)

        context = {
            'subpages': subpages,
            'page': page
        }
        html = render_to_string('subpage.html', {'context': context})
        return JsonResponse({'response': html})

def contentpage(request, page, subpage, content):
    if request.method == 'GET':
        page = PageContent.objects.get(menuname=content)

        context = {
            'page': page
        }
        return render(request, 'contentpage.html', {'context': context})

    if request.method == 'POST':
        page = PageContent.objects.get(menuname=content)

        context = {
            'page': page
        }
        html = render_to_string('contentpage.html', {'context': context})
        return JsonResponse({'response': html})