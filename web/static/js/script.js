
    var $fsm = document.querySelectorAll('.img-block');
    var $fsmActual = document.querySelector('#fsm_actual');
    if ($fsmActual) {
        $fsmActual.style.position = "absolute";
    }
    var position = {};
    var size = {};

    $('body').on('click', '.img-block', function(event) {
        console.log('click');
        setTimeout(function () {
            $('.loader').css('display', 'flex');
        }, 1000);
        // $(this).addClass('bigscreen');

        var $this = event.currentTarget;
        position = $this.getBoundingClientRect();
        size = {
            width: window.getComputedStyle($this).width,
            height: window.getComputedStyle($this).height
        };

        $fsmActual.style.position = "absolute";
        $fsmActual.style.top = $($this).position().top + 'px';
        $fsmActual.style.left = $($this).position().left + 'px';
        $fsmActual.style.height = size.height;
        $fsmActual.style.width = size.width;
        $fsmActual.style.margin = $this.style.margin;


        setTimeout(function(){
            $fsmActual.innerHTML = $this.innerHTML;
            var classes = $this.classList.value.split(' ');
            for (var i = 0; i < classes.length; i++) {
                $fsmActual.classList.add(classes[i]);
            }
            $fsmActual.classList.add('growing');
            $fsmActual.style.height = '100%';
            $fsmActual.style.width = '100%';
            $fsmActual.style.top = '0';
            $fsmActual.style.left = '0';
            $fsmActual.style.margin = '0';
            $('#fsm_actual').css('max-width', '100%');
            $('#fsm_actual').css('z-index', '30');
        }, 1);

        setTimeout(function(){
            $fsmActual.classList.remove('growing');
            $fsmActual.classList.add('full-screen')
        }, 1000);
    });

