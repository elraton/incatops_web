# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.PageContent)
class PageContentAdmin(admin.ModelAdmin):
    list_display = ('title', 'image_mini')
    def image_mini(self, obj):
        if obj.image:
            ret = u'<img style="max-height: 200px; max-width: 200px;" src="%s"/>' % obj.image.url
        else:
            ret = u'<b>Ops! There is no preview available</b>'
        return ret
    image_mini.allow_tags = True


class PageContentInlineAdmin (admin.TabularInline):
    model = models.PageContent
    extra = 0

@admin.register(models.Subpage)
class SubPageAdmin(admin.ModelAdmin):
    list_display = ('title', 'image_mini')
    inlines = [PageContentInlineAdmin, ]
    def image_mini(self, obj):
        if obj.image:
            ret = u'<img style="max-height: 200px; max-width: 200px;" src="%s"/>' % obj.image.url
        else:
            ret = u'<b>Ops! There is no preview available</b>'
        return ret
    image_mini.allow_tags = True

class SubPageInlineAdmin (admin.TabularInline):
    model = models.Subpage
    extra = 0

@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'image_mini')
    inlines = [SubPageInlineAdmin, ]
    def image_mini(self, obj):
        if obj.image:
            ret = u'<img style="max-height: 200px; max-width: 200px;" src="%s"/>' % obj.image.url
        else:
            ret = u'<b>Ops! There is no preview available</b>'
        return ret
    image_mini.allow_tags = True



