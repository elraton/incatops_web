# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.

class Page(models.Model):
    title    = models.CharField(max_length=100, verbose_name='Titulo')
    image    = models.ImageField(upload_to='pages', verbose_name='Imágen', blank=True, null=True)
    menuname = models.CharField(max_length=50, verbose_name='Nombre en el Menu')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Pagina Principal'
        verbose_name_plural = 'Pagina Principal'

class Subpage(models.Model):
    title = models.CharField(max_length=100, verbose_name='Titulo')
    image = models.ImageField(upload_to='pages', verbose_name='Imágen', blank=True, null=True)
    subpage = models.ForeignKey(Page, verbose_name='Pagina padre')
    menuname = models.CharField(max_length=50, verbose_name='Nombre en el Menu')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Subpaginas principales'
        verbose_name_plural = 'Subpaginas principales'

class PageContent(models.Model):
    title = models.CharField(max_length=100, verbose_name='Titulo')
    image = models.ImageField(upload_to='pages', verbose_name='Imágen', blank=False)
    text  = RichTextField(verbose_name='Texto', blank=False)
    subpage = models.ForeignKey(Subpage, verbose_name='Pagina padre')
    menuname = models.CharField(max_length=50, verbose_name='Nombre en el Menu')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Paginas con texto'
        verbose_name_plural = 'Paginas con texto'