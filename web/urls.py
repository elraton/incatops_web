from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = (
    url(r'^$', views.index, name='home'),

    #url(r'^ourstory/$', views.ourstory, name='ourstory'),

    #url(r'^ourgenes/$', views.ourgenes, name='ourgenes'),
    #url(r'^ourgenes/history/$', views.history, name='history'),
    #url(r'^ourgenes/misionvision/$', views.misionvision, name='misionvision'),
    #url(r'^ourgenes/values/$', views.values, name='values'),

    #url(r'^pacomarca/$', views.pacomarca, name='pacomarca'),
    #url(r'^pacomarca/herdsmanhut/$', views.herdsmanhut, name='herdsmanhut'),
    #url(r'^pacomarca/incashearing/$', views.incashearing, name='incashearing'),
    #url(r'^pacomarca/blackalpaca/$', views.blackalpaca, name='blackalpaca'),
    #url(r'^pacomarca/geneticprogram/$', views.geneticprogram, name='geneticprogram'),

    #url(r'^ourgenes/$', views.ourgenes, name='ourgenes'),


    url(r'^(\w+)/$', views.mainpage),
    url(r'^(\w+)/(\w+)/$', views.subpage),
    url(r'^(\w+)/(\w+)/(\w+)/$', views.contentpage),

)